FROM python:3.8.1-alpine
LABEL maintainer “remitoudic@gmail.com”
WORKDIR /ENGIE_CODING_CHALLENGE
ADD .  /ENGIE_CODING_CHALLENGE
RUN pip install -r requirements.txt
CMD ["python","main.py"]