from main import app
from flask import json
import example_payload.payload1 as payload1
import example_payload.payload2 as payload2
import example_payload.payload3 as payload3
import unittest
data_test = [payload1, payload2, payload3]


class TestApi(unittest.TestCase):
    """  test the  sum of the production of the powerplant proction plan """

    def test_sum_production(self):
        """ test sum production plan = loaf """
        for payload in data_test:
            response = app.test_client().post(
                '/productionplan', data=json.dumps(payload.data))

            data_result = json.loads(response.get_data(as_text=True))
            SUM = 0
            for prod_unit in data_result:
                SUM = SUM + prod_unit["p"]

            print("prod:", SUM, " load:", payload.data["load"])

            assert round(SUM) == payload.data["load"]
            assert response.status_code == 200


if __name__ == "__main__":
    unittest.main()
