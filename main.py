from flask import Flask
from flask import request
from flask import jsonify, json
import werkzeug
from production_managment import ProductionPlan

app = Flask(__name__)


@app.route('/productionplan', methods=['GET', 'POST'])
def find_powerplants_mix():
    if request.method == 'POST':
        payload = json.loads(request.data)
        plan = ProductionPlan(payload)
        return jsonify(plan.production_plan)
    else:
        return """<h1> Engie coding Challenge </h1>
                  <p> You should POST a load to get the powerplants mix </p>
                  <p> url: https://engiecodingchallenge.ew.r.appspot.com/productionplan  </p> """

# Some Error handeling


@app.errorhandler(werkzeug.exceptions.BadRequest)
def handle_bad_request(e):
    return 'bad request!'


@app.errorhandler(werkzeug.exceptions.NotFound)
def handle_not_found(e):
    return 'endpoint not found !'


app.register_error_handler(400, lambda e: 'bad request!')
app.register_error_handler(404, lambda e: 'NotFound')

if __name__ == "__main__":
    app.run(host="0.0.0.0", port=8888, debug=True)
