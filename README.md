#  ENGIE CODING CHALLENGE 

For the [challenge](https://github.com/gem-spaas/powerplant-coding-challenge/blob/master/README.md), Python and the webframework  Flask have been used.

The challenge constists of building an endpoint for calculating the
production plan for a certain amount of electricity ( the "load") using the merit order principle.


### How to build the endpoint ?
#### Localy:
```
        - 0. A working Python3 version with pip installed is assumed.

        - 1. Go the ENGIE_CODING_CHALLENGE folder with terminal.

        - 2. Install dependencies with the command : pip install -r requirements.txt

        - 3. Start the enpoint with the command : python3 main.py

        - 4. then you can POST a load at http://0.0.0.0:8888/productionplan

```
####  With docker:
```
        - 0. A working docker program is assumed.

        - 1. Go the ENGIE_CODING_CHALLENGE folder with the terminal.

        - 2. run command  : docker build -t engie_test_code .

        - 3. run command :   docker run -d -p 8888:8888 engie_test_code

        - 4. then you can POST a load at  http://0.0.0.0:8888/productionplan

```
####  Deployment & Testing :
```
For testing usage purposes the endpoint has been deployed on GCP (Google Could Paltform) with google app engine.

url : https://engiecodingchallenge.ew.r.appspot.com/productionplan
!!! Only post request so you need an hhttp client progam to do it !!!

Unit tests can be perform localy with the command : python -m unittest
```

###  About my solution:

The optimisation has been done  by first calculating the marginal cost of generating an unit of
electricity for each powerplant. Then the powerplants are ranked according to their marginal cost.
Following the ranking and the min. max. of production constraints, the powerplants are put into the production plan until their production volume is reached.

This way,  it minimizes the cost for  generating  the load of electricty needed.

About managing and loggging runtimes errors , I am  not sure  I understood
the task, but I attemped someting.

Thank you for the coding challenge it has been very interesting coding test.
I hope that I will pass at least submission.(cross fingers ...)


