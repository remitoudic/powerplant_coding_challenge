
class ProductionPlan:
    """ Class for calculating how much each powerplant
        participate to production plan.

        input:  json (the load)
        return : json (the production plan )
    """

    def __init__(self, data):
        self.fuels = data['fuels']
        self.powerplants = data['powerplants']
        self.load = data["load"]
        self.marginal_cost()
        self.merit_order_sort()
        self.production_mix()
        self.validating_data()

    def marginal_cost(self):
        """ - calculate the generation cost for an extra
              electricity unit for each  powerplant ( named:"m_cost")
            - Update self.powerplants with "m_cost".
        """
        for pplant in self.powerplants:

            if "gas" in pplant["type"]:
                inverse_eff = 1/pplant["efficiency"]
                m_cost = inverse_eff * self.fuels["gas(euro/MWh)"]
                pplant["m_cost"] = m_cost

            if "turbojet" in pplant["type"]:
                inverse_eff = 1/pplant["efficiency"]
                m_cost = inverse_eff * self.fuels["kerosine(euro/MWh)"]
                pplant["m_cost"] = m_cost

            if "wind" in pplant["type"]:
                pplant["m_cost"] = 0
                # Special rule for wind => adjust production max with wind(%)
                pplant["pmax"] = pplant["pmax"] * (self.fuels["wind(%)"]/100)

    def merit_order_sort(self):
        """ sort the powerplants list according to marginal cost """
        self.plant_eco_effecency = sorted(
            self.powerplants, key=lambda item: item['m_cost'])

    def production_mix(self):
        """ calculate the production plan """
        load = self.load
        plants_mix = list()

        for asset in self.plant_eco_effecency:

            # asset not usable
            if asset["pmax"] == 0:
                plants_mix.append({"name": asset["name"],
                                   "p": 0})
                continue

            # asset fully used
            if load > asset["pmin"] and load > asset["pmax"]:
                plants_mix.append({"name": asset["name"],
                                   "p": round(asset["pmax"], 1)})
                load = load-asset["pmax"]
                continue

            # asset partly use
            elif load > asset["pmin"] and load < asset["pmax"]:
                plants_mix.append({"name": asset["name"],
                                   "p": round(load, 1)})
                load = 0

            # asset not used
            else:
                plants_mix.append({"name": asset["name"],
                                   "p": 0})

        self.production_plan = plants_mix
        return plants_mix

    def production_mix_adjust(self):

        # 1) find  min prod  with prod max!= 0
        seq = [x['pmin']
               for x in self.powerplants if x['pmin'] and x['pmax'] > 0]
        min(seq)

        # 2)  Adjust first order max
        self.powerplants[0]["pmax"] = self.powerplants[0]["pmax"] - min(seq)

        # 3) retourner l'ago de nouveau
        self.production_mix()

    def validating_data(self):
        self.validated_data = False

        # checking sum  of "p" egal load
        SUM = 0
        for prod_unit in self.production_plan:
            SUM = SUM + prod_unit["p"]
        if float(SUM) == float(self.load):
            self.validated_data = True
        else:
            self.production_mix_adjust()


# # for standalone debugging
# if __name__ == "__main__":
#     import pprint
#     pp = pprint.PrettyPrinter(indent=4)
#     test = ProductionPlan(data)

#     # production plan
#     pp.pprint(test.production_plan)
